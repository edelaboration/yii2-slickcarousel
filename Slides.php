<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\slickcarousel;

/**
 * Description of Slides
 *
 * @author Tartharia
 */
class Slides extends \yii\widgets\ListView{
    
    public function init() {
        $this->layout='{items}';
        parent::init();
    }

    public function run() {
        if ($this->showOnEmpty || $this->dataProvider->getCount() > 0) {
            $content = preg_replace_callback("/{\\w+}/", function ($matches) {
                $content = $this->renderSection($matches[0]);

                return $content === false ? $matches[0] : $content;
            }, $this->layout);
        } else {
            $content = $this->renderEmpty();
        }
        
        echo $content;
    }
}
