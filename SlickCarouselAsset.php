<?php

namespace robote13\slickcarousel;

use yii\web\AssetBundle;

class SlickCarouselAsset extends AssetBundle
{
    public $sourcePath = '@bower/slick-carousel/slick';

    public $depends = [
        'yii\web\JqueryAsset'
    ];
    
    public $publishOptions=[
        'only'=>[
            '*.css',
            '*.js',
            '*.gif',
            'fonts/*.*'
        ]
    ];
    
    public function init() {
        parent::init();
        $this->depends = [
        'yii\web\JqueryAsset'
    ];
        $this->css = [
            'slick.css'
        ];
        
        $this->js = [
            YII_DEBUG?'slick.js':'slick.min.js'
        ];
    }
}