<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\slickcarousel;

use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

/**
 * Description of SlickCarouselWidget
 *
 * @author Tartharia
 */
class SlickCarouselWidget extends \yii\base\Widget{

    public $options = [];

    public $container = 'div';

    public $clientOptions = [];

    private $_defaultClientOptions = ['infinite'=> false,'mobileFirst'=>true];

    private $hashVar;


    public function init() {
        if(!isset($this->options['id']))
        {
            $this->options['id'] =  $this->id;
        }
        else{
            $this->id = $this->options['id'];
        }
        Html::addCssClass($this->options,'slick-init');
        $this->clientOptions = ArrayHelper::merge($this->_defaultClientOptions, $this->clientOptions);
        $this->storePluginOptions();
        echo Html::beginTag($this->container,$this->options);
    }

    public function run() {
        echo Html::endTag($this->container);
        $this->registerClientScript();

    }

    protected function storePluginOptions()
    {
        $this->hashVar = "slick_" . hash('crc32', Json::htmlEncode($this->clientOptions));
        $this->options['data']=['robote13-slick'=>$this->hashVar];
    }

    protected function registerClientScript()
    {
        $view = $this->getView();
        SlickCarouselAsset::register($view);
        $options = Json::encode($this->clientOptions);
        $view->registerJs( <<<JS
            jQuery('[data-robote13-slick="{$this->hashVar}"]').removeClass('slick-init').slick({$options});
            window.{$this->hashVar} = {$options};\n
JS
        , View::POS_END,"sc_{$this->hashVar}");
    }
}
