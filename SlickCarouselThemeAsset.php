<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\slickcarousel;

/**
 * Description of SlickCarouselThemeAsset
 *
 * @author Tartharia
 */
class SlickCarouselThemeAsset extends \yii\web\AssetBundle{
    
    public $sourcePath = '@bower/slick-carousel/slick';
    
    public function init() {
        $this->css = [YII_DEBUG ?'slick-theme.css':'slick-theme.min.css'];
        parent::init();
    }
}
