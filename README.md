Slick Carousel
==============

The Slick Carousel widget is a Yii2 wrapper for the http://kenwheeler.github.io/slick/

For usage standart Slick Carousel theme add dependency in config:


```
#!php
'components'=>[
    'assetManager'=>[
        'bundles'=>[
            ... ,
            'robote13\slickcarousel\SlickCarouselAsset'=>[
                'depends'=>[
                    'robote13\slickcarousel\SlickCarouselThemeAsset'
                ]
            ]
        ]
    ]
]
```